-- andromeda
-- ciron 9?

--[[

    so a long time ago i started making tablet admins under the name ciron because
    i thought it was cool or something? i dont know

    anyway i found a new script builder and i was looking through the old ciron
    generations and inspiration struck since the code was so awful

    so here's a new ciron with (hopefully) innovative features

]]--

predicate = {
    -- true when v is compared to x
    -- y(x) -> bool
     eq = function(x) return function(v) return v == x end end,
    neq = function(x) return function(v) return v ~= x end end,
    
     gt = function(x) return function(v) return v >  x end end,
     lt = function(x) return function(v) return v <  x end end,
    geq = function(x) return function(v) return v >= x end end,
    leq = function(x) return function(v) return v <= x end end,

    either = function(a, b) return function(x) return a(x) or b(x) end end,
    both = function(a, b) return function(x) return a(x) and b(x) end end,

    -- true when v[p] is compared to x
    -- prop_y(p, x) -> bool
     prop_eq = function(p, x) return function(v) return v[p] == x end end,
    prop_neq = function(p, x) return function(v) return v[p] ~= x end end,

     prop_gt = function(p, x) return function(v) return v[p] >  x end end,
     prop_lt = function(p, x) return function(v) return v[p] <  x end end,
    prop_geq = function(p, x) return function(v) return v[p] >= x end end,
    prop_leq = function(p, x) return function(v) return v[p] <= x end end,

    contains = function(x) return function(v) return contains(v, x) end end,
    prop_contains = function(p, x) return function(v) return contains(v[p], x) end end,

}

-- returns true if t contains x
contains = function(t, x)
    for i,v in next, t do
        if v == x then
            return true
        end
    end
    return false
end

-- returns true if s contains x
string_contains = function(s, x)
    for i = 1, #s - (#x - 1) do
        if s:sub(i, i + #x - 1) == x then
            return true
        end
    end
    return false
end

-- gets the index of x in the string s, or -1 if it isn't present
string_index_of = function(s, x)
    for i = 1, #s - (#x - 1) do
        if s:sub(i, i + #x - 1) == x then
            return i
        end
    end
    return -1
end

-- creates a new array of items satisfying the predicate p in table t
where = function(t, p)
    local n = {}
    for i,v in next, t do
        if p(v, i) then
            table.insert(n, v)
        end
    end
    return n
end

-- returns true if any item in t passes the predicate p
any = function(t, p)
    for i,v in next, t do
        if p(v, i) then
            return true
        end
    end
    return false
end

-- calls the function f for every item in t
each = function(t, f)
    for i,v in next, t do
        f(v, i)
    end
end

-- reverses the array t
reverse = function(t)
    local n = {}
    for i = #t, 1, -1 do
        table.insert(n, t[i])
    end
    return n
end

-- calls the function for every item in reverse(t)
each_reverse = function(t, f)
    for i = #t, 1, -1 do
        f(t[i], i)
    end
end

-- maps each item of t to a new item by function f. if the return type is a tuple, the second item is used as the indedx
map = function(t, f)
    local n = {}
    for i,v in next, t do
        local x, y = f(v, i)
        n[y or i] = x
    end
    return n
end

-- separates t into groups of c
in_groups_of = function(t, c)
    local groups = {}
    local groupnum = 1
    for i,v in next, t do
        groupnum = math.floor((i - 1) / c) + 1
        if not groups[groupnum] then groups[groupnum] = {} end
        table.insert(groups[groupnum], v)
    end
    return groups
end

-- sums the table t, assuming they can be added
sum = function(t)
    local s = 0
    each(t, function(x) s = s + x end)
    return s
end

-- removes items from t that satisfy predicate p
remove_where = function(t, p)
    for i = #t, 1, -1 do
        if p(t[i], i) then
            table.remove(t, i)
        end
    end
end

-- removes items from t at the given indices
remove_at = function(t, ...)
    local ind = {...}
    for i = #t, 1, -1 do
        if contains(ind, i) then
            table.remove(t, i)
        end
    end
end

-- creates a unique table excluding duplicates from table t
unique = function(t)
    local n = {}
    for i,v in next, t do
        if not contains(n, v) then
            table.insert(n, v)
        end
    end
    return n
end

-- merges the tables a and b consecutively to form one table
merge = function(a, b)
    local n = {}
    each(a, function(x) table.insert(n, x) end)
    each(b, function(x) table.insert(n, x) end)
    return n
end

-- returns true if the table t is completely empty (e.g. #t == 0 and t[x] == nil where x is any index)
empty = function(t)
    for i,v in next, t do
        return false
    end
    return true
end

-- gets the index of x in the table t, or -1 if it is not present
index_of = function(t, x)
    for i,v in next, t do
        if v == x then
            return i
        end
    end
    return -1
end

-- equivalent to where(t, p)[1], but returns a tuple where the second item is the index of the matched object in the original table
find = function(t, p)
    local matches = where(t, p)
    local match = #matches > 0 and matches[1] or nil
    return match, index_of(t, match)
end

-- splits a text up by words, counting quoted expressions as one word
split_words = function(text)
    if #text == 0 then return {} end

    local words = {}
    local spat, epat, buf, quoted = [=[^(['"])]=], [=[(['"])$]=]
    for str in text:gmatch("%S+") do
        local squoted = str:match(spat)
        local equoted = str:match(epat)
        local escaped = str:match([=[(\*)['"]$]=])
        if squoted and not quoted and not equoted then
            buf, quoted = str, squoted
        elseif buf and equoted == quoted and #escaped % 2 == 0 then
            str, buf, quoted = buf .. ' ' .. str, nil, nil
        elseif buf then
            buf = buf .. ' ' .. str
        end
        if not buf then table.insert(words, (str:gsub(spat,""):gsub(epat,""))) end
    end
    if buf then warn("Missing matching quote for "..buf) end
    return words
end

-- splits a text up by a separator
split = function(text, separator)
    local t = {}
    for x in string.gmatch(text, "([^" .. separator .. "]+)") do
        table.insert(t, x)
    end
    return t
end

-- equivalent to select('#', unpack(t))
after = function(t, x)
    local n = {}
    for i,v in next, t do
        if i >= x then
            table.insert(n, v)
        end
    end
    return n
end

-- gets the range [low, high] from t
range = function(t, low, high)
    local n = {}
    for i,v in next, t do
        if i >= low and i <= high then
            table.insert(n, v)
        end
    end
    return n
end

-- rejoins words separated by split_words
join_args = function(args)
    local a = {}
    for i,v in next, args do
        a[i] = v:gsub("\"", "\\\"")
        if string_contains(a[i], " ") then
            a[i] = "\"" .. a[i] .. "\""
        end
    end
    return table.concat(a, " ")
end

-- returns a table of c length, all element e
constant_repeat = function(c, e)
	local n = {}
	for i = 1, c do
		table.insert(n, e)
	end
	return n
end

-- removes the first element until it is unique consecutively
first_unique = function(t)
    local n = {}
    local f = t[1]
    local s = false
    for i,v in next, t do
        if v ~= f or s then
            table.insert(n, v)
            s = true
        end
    end
    return #n == 0 and {f} or n
end

-- pads the table t so it becomes length c with element e
left_pad = function(t, c, e)
	return merge(constant_repeat(c - #t, e), t)
end

-- linear interpolation
lerp = function(a, b, c) return a + (b - a) * c end

service = function(x) return game:GetService(x) end
exists = function(x) return x and x.Parent end

match_players = function(speaker, text)
    local matches = {}
    each(map(split(text, ","), string.lower), function(x)
        if x == "me" then
            table.insert(matches, speaker)
        elseif x == "all" then
            each(service("Players"):GetPlayers(), function(p) table.insert(matches, p) end)
        elseif x == "others" then
            each(where(service("Players"):GetPlayers(), predicate.neq(speaker)), function(p) table.insert(matches, p) end)
        else
            each(service("Players"):GetPlayers(), function(p)
                if p.Name:lower():sub(1, #x) == x:lower() then
                    table.insert(matches, p)
                end
            end)
        end
    end)
    return unique(matches)
end

play_sound = function(id, parent, pitch)
    local sound = Instance.new("Sound")
    sound.SoundId = "rbxassetid://" .. id
    sound.Pitch = pitch or 1
    sound.Parent = parent or workspace
    sound:Play()
    delay(sound.TimeLength, function()
        sound:Stop()
        sound:Destroy()
    end)
end

hsv_to_rgb = function(h, s, v)
    local r, g, b

    local i = math.floor(h * 6);
    local f = h * 6 - i;
    local p = v * (1 - s);
    local q = v * (1 - f * s);
    local t = v * (1 - (1 - f) * s);

    i = i % 6

    if i == 0 then r, g, b = v, t, p
    elseif i == 1 then r, g, b = q, v, p
    elseif i == 2 then r, g, b = p, v, t
    elseif i == 3 then r, g, b = p, q, v
    elseif i == 4 then r, g, b = t, p, v
    elseif i == 5 then r, g, b = v, p, q
    end

    return r * 255, g * 255, b * 255
end

in_range = function(n, low, high)
    return n >= low and n <= high
end

--------------------------------------------------------------------

-- Extracts 8 bits from a character, big-endian.
bits_from_char = function(char)
    local num = string.byte(char)

    local t = {}
    for i = 7, 0, -1 do
        t[#t + 1] = math.floor(num / 2 ^ i)
        num = num % 2 ^ i
    end
    return left_pad(t, 8, 0)
end

-- Establishes a number from a list of bits, big-endian.
num_from_bits = function(bits)
    return sum(map(reverse(bits), function(b, i) return b * math.pow(2, i - 1) end))
end

-- Establishes a number from a list of bytes, each big-endian.
num_from_bytes = function(bytes)
    return sum(map(reverse(bytes), function(b, i) return b * math.pow(16, i - 1) end))
end

-- A bit stream is a stream that can process a string of characters and extract bits and bytes from it.
bit_stream = {}
bit_stream.new = function(str)
    local stream = {}

    stream.str = str
    stream.char = 1
    stream.char_pos = 1
    stream.clock = 0
    
    setmetatable(stream, {__index = bit_stream})
    return stream
end

-- Takes <count> bits from the stream.
bit_stream.next_bits = function(self, count, no_clock_update)
    assert(self.char <= #self.str, "stream is empty")
    if not no_clock_update then self.clock = self.clock + count end
    local bits = bits_from_char(self.str:sub(self.char, self.char))
    
    local to = self.char_pos + count - 1
    local result = range(bits, self.char_pos, math.min(to, 8))
    self.char_pos = to + 1
    if self.char_pos > 8 then
        self.char_pos = 1
        self.char = self.char + 1

        if count - #result > 0 then result = merge(result, self:next_bits(count - #result, true)) end
    end

    return result
end

-- Takes the next bit from the stream.
bit_stream.next_bit = function(self)
    return (self:next_bits(1) or {})[1]
end

-- Takes the next number from the stream, based on the number of bits provided.
bit_stream.next_num = function(self, bits)
    return num_from_bits(self:next_bits(bits))
end

-- Takes the next <count> bytes from the stream.
bit_stream.next_bytes = function(self, count)
    return map(in_groups_of(self:next_bits(count * 8), 8), num_from_bits)
end

-- Takes the next <count> chars from the stream, by taking their bytes and creating an ASCII character from them.
bit_stream.next_chars = function(self, count)
    return map(self:next_bytes(count), function(b) return string.char(b):sub(1, 1) end)
end

-- Takes the next <count> chars from the stream and appends them into a string.
bit_stream.next_string = function(self, count)
    return table.concat(self:next_chars(count), "")
end

-- Takes the next variable-length quantity from the stream. Big-endian, appropriate for MIDI decoding.
bit_stream.next_vlq = function(self)
    local quantity = {}
    local next_byte
    
    repeat
        next_byte = self:next_bit() == 1
        quantity = merge(quantity, self:next_bits(7))
    until not next_byte
    
    return num_from_bits(quantity)
end

--------------------------------------------------------------------

midi = {}

-- Establishes a list of IDs and properties for MIDI programs.
do
    snap = 12222140
    ping = 12221990
    bass = 12221831
    button = 12221967
    clickf = "rbxasset://sounds//clickfast.wav"
    clunk = "rbxasset://sounds//flashbulb.wav"

    -- {id, tune from key 60, min key, max key, attack pos, sustain time, loop}

    -- piano
    local piano = {
        {"4769804444", 24, 0, 35, 1.6, 0},
        {"4769804661", 12, 36, 45, 1.6, 0},
        {"4769804828", 0, 46, 57, 1.6, 0},
        {"4769805072", -12, 58, 74, 1.6, 0},
        {"4769805286", -24, 75, 127, 1.6, 0}
    }

    midi.instruments = {
    --{{"12857654",0.3,0,66,0.5,0.06},{"12857804",-7,67,71,0.5,0.03},{"12857664",-11.5,72,127,0.8,0.02}}, 
    piano,
    {{"12857637",-3.9,0,64,0.5},{"12857660",-8.4,65,127,0.5}}, 
    {{"13114759",-6,0,127,1}}, 
    {{"19344667",-8.6,0,127,0.5}}, 
    {{"15666462",-13.5,0,127,0.4}}, 
    {{"13424334",-23.2,0,127,0.5,0,0.5}}, 
    {{"1089407",8.2,0,57,0.5},{"1089403",3.2,58,127,1}},
    {{"13032199",8.2,0,57,0.5},{"13032178",3.2,58,127,1}}, 
    {{"60661796",-2.5,0,127,0.5,0.3,0.2}}, 
    {{"13019281",11.2,0,127,0.5}}, 
    {{"13019300",11.3,0,127,0.5}}, 
    {{"12892216",-7,0,127,0.5}}, 
    {{"10209668",6.5,0,127,0.5}}, 
    {{"11998777",12,0,127,0.5}},
    {{"11998796",4.8,0,127,0.5,0,0.15}}, 
    {{bass,11.7,0,127,0.5}}, 
    {{"rbxasset://sounds//uuhhh.wav",0.2,0,127,0.5}}, 
    {{button,6.7,0,127,0.4}}, 
    {{ping,-21.7,0,127,0.5,0,0.2}}, 
    {{"rbxasset://sounds//Kid saying Ouch.wav",0.9,0,127,0.5}},
    {{"11984351",16.8,0,127,0.3,0.02,0.2}}, 
    {{"13114759",39.7,26,26,0.5},{snap,37,27,27,0.5,0,0.25},
    {"108553903",38,28,28,0.5,0,0,0},
    {"18426149",41,29,29,0.4,0,0.25},{"18426149",34,30,30,0.4,0,0.25},{snap,37,31,31,0.2},
    {clickf,40,31,31,0.7},{clunk,40,32,32,0.5},{clickf,38,33,34,0.5},
    {"56524816",24,34,34,0.3},{"31173820",20,35,35,0.5},{"31173820",24,36,36,0.5},
    {"14863866",16,37,37,1},{"31173799",20,38,38,0.7},{"2101148",54,39,39,0.7},{"31173799",20,40,40,0.5},
    {"31173881",17,41,41,0.5},
    {snap,45,42,42,0.25,0,0.25},{"31173881",19,43,43,0.5},{snap,52,44,44,0.25,0,0.25},
    {"31173881",21,45,45,0.5},{"31173735",14,46,46,0.5},{"31173881",23,47,47,0.5},{"31173844",13,48,48,0.5},
    {"31173771",14,49,49,0.5},{"31173844",15,50,50,0.5},{"31173898",8,51,51,0.5},{"31173771",5,52,52,0.5},
    {"11113679",9.5,53,53,0.5},{snap,34,54,54,0.15,0,0.25},{"31173771",24,55,55,0.5},{snap,14,56,56,0.2},{"57802055",1,56,56,0.3},
    {"31173771",7,57,57,0.5},{"31173898",1,59,59,0.5},{"57801983",3,60,60,0.5,0,0.25},{"57801983",-10,61,61,0.5,0,0.25}, -- tuning was -5
    {"101088307",24,62,62,0.5,0,0,0},{"57802055",-16,63,63,0.5,0,0.25},{"57802055",-22,64,64,0.5,0,0.25},{"57801983",-15,65,65,0.5,0,0.25}
    ,{"57801983",-21,66,66,0.5,0,0.25},
    {"13114759",-8,67,67,0.5,0,0.25},{"13114759",-14,68,68,0.5,0,0.25},
    {snap,22,69,69,0.4},
    {snap,32,70,70,0.5,0,0.25},{"28510547",-14.4,71,71,0.4,0,0.25},{"28510547",-18.4,72,72,0.4,0,0.25},{"101180005",-3.4,73,73,0.3,0,0,0},
    {"100391463",-4,74,74,0.4,0,0,0},{"15666462",8,75,75,0.4},{"57802055",-2,76,76,0.4},{"57802055",-5,77,77,0.4},{"75338648",-13,78,78,0.6,0,1,0},{"75338648",-31.7,79,79,0.5,0,0,0},
    {"19344667",2.5,80,80,0.25,0,0.25},{"19344667",1.5,81,81,0.2},{snap,12,82,82,0.3,0,0.25},{"101093314",-22,83,83,0.4,0,0.25},
    {"13061809",-4,84,84,0.4},{"57802055",8,85,85,0.4},{"31173844",-35,86,86,0.4},{"31173844",-36,87,87,0.4}},
    {{"13061809",-2,56,64,0.5},{"13061802",-10,65,74,0.5},{"13061810",-23,75,84,0.5},{"11984254",-34,85,127,0.5},
    {"rbxasset://sounds//HalloweenThunder.wav",8,48,55,0.5},{"rbxasset://sounds//HalloweenGhost.wav",27,0,47,0.5}},
    {{"21338895",0,0,127,0.5}}, 
    {{"11949128",10,0,127,0.5}}, 
    {{"10209888",13.4,0,127,0.3},{"10209888",14.2,61,127,0.3}}, 
    {{"45885030",1.4,0,127,0.3}}, 
    {{"75421988",33.75,0,80,0.3,0,0.2,1},{"75338648",-24.7,81,127,0.5,0,0.4,1},
    {"75338648",-0.7,81,127,0.4,0,0.4,1},{"75338648",11.4,81,127,0.5,0,0.4,1}}, 
    {{"15666462",52.2,0,127,0.4,0.5,0.5,1}}, 
    {{"11944350",-9.6,0,127,0.5}}, 
    {{"79236960",0,0,127,0.3,0}}, 
    {{"81146867",16,0,127,0.3,0}}, 
    {{"75338648",-13,0,127,0.7,0,0.1,1}}, 
    {{"11949128",43,0,127,0.5,0,0.25,1}}, 
    {{"11998796",4.8,0,127,1,0,0.25},{"75421988",33.8,0,80,0.2,0.4,0.4,1},{"75421988",33.9,0,80,0.2,0.4,0.4,1},
    {"75338648",-24.7,81,127,0.5,0,0.4,1},{"75338648",-0.7,81,127,0.4,0,0.4,1},{"75338648",11.4,81,127,0.5,0,0.4,1}},
    {{"58479648",15.2,0,127,0.5,0,0.25}}, 
    {{"58479648",15.2,0,127,0.5,0,0.15,1}}, 
    {{"69446845",13.5,0,127,0.5,0,0.15,1}}, 
    {{"75421988",33.7,0,80,0.2,0.4,0.4,1},{"75421988",33.8,0,80,0.2,0.4,0.4,1},{"75338648",-24.7,81,127,0.5,0,0.4,1},{"75338648",-0.7,81,100,0.4,0,0.4,1},{"75338648",11.4,81,86,0.5,0,0.4,1}}, 
    {{"57802055",-2,0,127,0.5}},
    {{bass,23.8,0,127,0.5}}, 
    {{"25641508",-9.5,0,127,0.5}},
    {{bass,23.8,42,50,0.07},{"12857664",-11.6,36,80,0.6},
    {ping,-21.7,82,127,0.5,0,0.15},
    {"12857664",-11.6,87,127,0.18},{"12857664",-11.6,81,86,0.3},{bass,23.8,0,41,0.2},
    {ping,-21.7,0,81,0.8,0,0.2}},
    {{"75338648",-24.7,0,127,0.7,0,0.1,1}}, 
    {{"89357191",21.5,0,127,0.7,0,0.1,1}}, 
    {{"13417380",-18.3,0,127,0.7,0,0.1}}, 
    {{"13418521",-8,0,127,0.7,0,0.2}}, 
    {{"13414759",4,0,127,0.7,0.5,0.5}}, 
    {{"13414758",14,0,127,0.7,0,0.1,1}}, 
    {{"13414749",19,0,127,0.7,0,0.1,1}}, 
    {{"75338648",-24.7,0,127,0.5,0,0.1,1},{"75338648",-12.7,0,127,0.5,0,0.1,1},{"75338648",-5.7,0,127,0.7,0,0.1,1},{"75338648",-0.7,0,127,0.5,0,0.1,1}}, 
    {{"75338648",-24.7,0,127,0.6,0,0.5,1},{"75338648",-12.7,0,127,0.6,0,0.5,1},{"75338648",-5.7,0,127,0.5,0,0.5,1}}, 
    {{"75338648",-24.7,0,90,0.5,0,0.1,1},{"75338648",-5.7,0,90,0.4,0,0.1,1},{"75338648",-0.7,0,90,0.5,0,0.1,1},
    {"75338648",-24.7,91,127,0.25,0,0.1,1},{"75338648",-5.7,91,127,0.15,0,0.1,1},{"75338648",-0.7,91,127,0.25,0,0.1,1}},
    {{"75338648",-24.7,0,127,0.5,0,0.1,1},{"75338648",-0.7,0,127,0.4,0,0.1,1},{"75338648",11.4,0,127,0.5,0,0.1,1}}, 
    {{ping,-21.7,0,127,0.3,0,0.1},{ping,4.7,0,127,0.2,0,0.1},{ping,14.7,0,127,0.2,0,0.1}}, 
    {{ping,-21.7,0,127,0.5,0,0.25},{ping,14.4,0,127,0.2,0,0.25},{"13114759",6,0,127,0.2,0,0.25}}, 
    {{bass,23.8,0,127,0.5,0,0.5}}, 
    {{"55741744",2.8,54,127,0.5,0,0.5},{"55741275",13.8,0,53,0.5,0,0.5}}, 
    {{"60871617",-13,0,127,0.5,0,0.5}}, 
    {{"56524816",-13.8,0,127,0.5,0,0.5}}, 
    {{"31173844",2,0,127,0.5}}, 
    {{"75421988",33.6,0,80,0.2,0.05,0.4,1},{"75421988",33.7,0,80,0.2,0.05,0.4,1},{"75338648",-24.7,81,127,0.5,0.05,0.4,1},{"75338648",-0.7,81,100,0.4,0.05,0.4,1},{"75338648",11.4,81,86,0.5,0.05,0.4,1}}, 
    {{"57802055",-14,0,127,0.5,0,0.1},{"11949128",10,0,127,0.3,0,0.1}},
    {{"99190550",7.6,0,127,0.3,0,0.1}}, 
    {{"99170481",8,0,49,0.5,0,0.1},{"99170583",5.1,50,56,0.5,0,0.1},{"99190216",-4,57,66,0.5,0,0.1},{"99170403",-14,67,127,0.5,0,0.1}}, 
    {{"99666917",2,0,127,0.5,0,0.1}}, 
    {{"13414749",-5,0,127,0.4,0,0.2,0},{"75338648",-24.7,0,127,0.5,0,0.1,1}},
    {{"10209257",0,57,127,0.2,0,0,0},{"10209257",0,47,56,0.15,0,0,0},{"10209257",0,0,46,0.1,0,0,0}},
    {{"106708146",20,0,127,0.4,0.1,0,1}}, 
    {{"75338648",-24.7,0,127,1,0.5,0.1,1},{"75338648",-5.7,0,127,0.2,0.5,0.1,1},{"75338648",-12.7,0,127,0.15,0.5,0.1,1}},
    {{"75338648",-24.7,0,127,0.7,0,0.1,1},{"75338648",23.8,0,127,0.01,0,0.1,1},{"75338648",-12.7,0,127,0.1,0,0,1}}, 
    {{"108553955",17.1,0,127,0.5,0,0,1},{"75338648",-24.7,46,127,0.4,0,0.1,1},{"75338648",-24.7,0,45,0.7,0,0.1,1}}, 
    {{"109618842",-24,70,127,0.4,0,0,0},{"109618754",-14,0,56,0.4,0,0,0},{"109618435",-18.8,66,69,0.4,0,0,0},{"109619047",-23,57,59,0.4,0,0,0},{"109618940",-11.9,60,65,0.4,0,0,0}}, 
    {{"109618842",-24,86,127,0.4,0,0,0},{"109618754",-14,0,59,0.4,0,0,0},{"109618940",-11.9,60,85,0.4,0,0,0}}, 
    {{"109618435",-18.8,0,127,0.6,0,0.2,0}}, 
    {{"109619047",-23,0,127,0.4,0,0.2,0}}, 
    {{"12222140",60,27,27,0.5,0,0,0},{"12222140",60,28,28,0.5,0,0,0},
    {"31173735",31,29,29,0.4,0,0,0},{"31173898",24,30,30,0.5,0,0,0},{snap,37,31,31,0.2},
    {clickf,40,31,31,0.7},{clunk,40,32,32,0.5},{clickf,38,33,34,0.5},
    {"56524816",24,34,34,0.3},{"31173820",20,35,35,0.5},{"11949128",10,36,36,0.5,0,0,0},
    {"14863866",16,37,37,1},{"112503990",28,38,38,0.8,0,0,0},
    {"106543491",20.5,39,39,0.5,0,0,0},{"112503990",26.4,40,40,0.8,0,0,0},
    {"11949128",10,41,53,0.5,0,0,0},{snap,34,54,54,0.15,0,0.25},{"31173771",24,55,55,0.5},
    {snap,14,56,56,0.2},{"57802055",1,56,56,0.3},{"31173771",12,57,57,0.5,0,0,0},
    {"31173771",16,59,59,0.5,0,0,0},
    {"57801983",3,60,60,0.5,0,0.25},{"57801983",-10,61,61,0.5,0,0.25},
    {"101088307",24,62,62,0.5,0,0,0},{"57802055",-16,63,63,0.5,0,0.25},
    {"57802055",-22,64,64,0.5,0,0.25},{"57801983",-15,65,65,0.5,0,0.25},
    {"57801983",-21,66,66,0.5,0,0.25},{"13114759",-8,67,67,0.5,0,0.25},
    {"13114759",-14,68,68,0.5,0,0.25},
    {snap,22,69,69,0.4},{snap,32,70,70,0.5,0,0.25},{"28510547",-14.4,71,71,0.4,0,0.25},
    {"28510547",-18.4,72,72,0.4,0,0.25},{"101180005",-3.4,73,73,0.3,0,0,0},
    {"100391463",-4,74,74,0.4,0,0,0},{"15666462",8,75,75,0.4},{"57802055",-2,76,76,0.4},
    {"57802055",-5,77,77,0.4},{"75338648",-13,78,78,0.6,0,1,0},{"75338648",-31.7,79,79,0.5,0,0,0},
    {"19344667",2.5,80,80,0.25,0,0.25},{"19344667",1.5,81,81,0.2},{snap,12,82,82,0.3,0,0.25},
    {"101093314",-22,83,83,0.4,0,0.25},
    {"13061809",-4,84,84,0.4},
    {"57802055",8,85,85,0.4},
    {"31173844",-35,86,86,0.4},
    {"31173844",-36,87,87,0.4}},
    {{"75338648",-24.9,0,67,0.5,0,0.1,0},{"12221990",2.3,0,127,0.5,0,0.1,0},
    {"12221990",-9.7,0,69,0.5,0,0.1,0},{"12221990",6,0,127,0.4,0,0.1,0},
    {"75338648",-24.8,68,127,0.5,0,0.1,1},{"75338648",-12.8,70,127,0.7,0,0.2,1}},
    {{"109618435",-18.8,0,127,0.4,0,0.1,0},{"75338648",-24.7,0,127,0.7,0,0.1,1}},
    {{"12221831",23.8,61,127,0.4,0,0.5,0},{"11949128",10,0,127,0.4,0,0.1,0},
    {"12221831",23.8,0,60,0.3,0,0.5,0}},
    {{"108553903",0,0,127,0.5,0,0,0}}, 
    {{"129562296",-9,0,127,0.17,0,0.4,1}}, 
    {{"131030037",12,0,127,0.3,0,0.4,1}}, 
    {{"129562240",-9,0,127,0.23,0,0.5,1}}, 
    {{"129562296",-8.9,0,127,0.15,0,0.25,1},{"129562296",-9.1,0,127,0.15,0,0.25,1}}, 
    {{"131030037",11.95,0,127,0.3,0,0.2,1},{"131030037",12.05,0,127,0.3,0,0.2,1}},
    {{"129562240",-9,0,127,0.3,0.4,0.1,1},{"129562240",10,0,127,0.05,0.4,0.1,1},{"129562240",3,0,127,0.04,0.4,0.1,1}}, 
    {{"9413306",-10,0,127,0.5,0,0,0.5,0}}, 
    {{"130818484",-12,0,127,0.9,0,0.25,0}},
    {{"130849959",12,0,89,0.8,0,0.4,1},
    {"130849959",12,90,102,0.5,0,0.4,1}}, 
    {{"130850059",12,0,89,0.8,0,0.4,1},
    {"130850059",12,90,102,0.5,0,0.4,1}}, 
    {{"130851174",0,0,127,0.25,0,0.4,0}}, 
    {{"130850183",0,53,127,0.7,0,0,0}, 
    {"130850227",24,0,52,0.3,0,0,0}}, 
    {{"130850974",2,0,127,0.4,0,0.25,0}}, 
    {{"130944009",0,0,127,0.5,0,0,0}}, 
    }
end

-- Gets the MIDI instrument based on the numeric program and note to be played. Otherwise, returns the default instrument.
midi.get_instrument = function(program, note)
    local instrument_pack = midi.instruments[program] or midi.instruments[1]
    for i,v in next, instrument_pack do
        if in_range(note, v[3], v[4]) then
            return v
        end
        if i == #instrument_pack then
            return v
        end
    end
    return midi.default_instrument
end

midi.default_instrument = midi.instruments[1][1]

-- Creates a new MIDI decoder/player from the source.
midi.new = function(source)
    local m = {}

    m.source = source:gsub("\r\n", "\n"):gsub("\n\r", "\n")
    m.stream = bit_stream.new(source)
    m.tempo = 500000
    m.signature = {4, 4}
    m.volume = 1
    m.tempo_multiplier = 1
    m.channels = {}
    m.tracks = {}

    for i = 1, 16 do
        table.insert(m.channels, {
            program = 1,
            sounds = {}
        })
    end

    setmetatable(m, {__index = midi})
    return m
end

-- Represents the currently playing MIDI track.
midi.playing = nil

-- Plays the MIDI object. It must be decoded using midi:decode() first.
midi.play = function(self)
    midi.playing = self

    each(self.tracks, function(track)
        -- track init
        track.clock = 0
        track.event = 1
    end)

    local function event_delta_time(ev)
        return (self.tempo / self.ticks_per_quarter_note) * ev.delta_time / math.pow(10, 6) + (ev.type == "smpte offset" and ev.time_offset or 0)
    end

    local previous_time = time()
    local heartbeat = service("RunService").Heartbeat
    repeat
        local delta = (time() - previous_time) * self.tempo_multiplier

        for i,v in next, self.tracks do
            if v.complete or (self.format_type == 2 and i > 1 and not self.tracks[i - 1].complete) then
                -- in format type 2, we must wait for each consecutive track to complete
            else
                if v.event > #v.events then
                    v.complete = true
                else
                    v.clock = v.clock + delta
                    local event, channel_id, channel, event_wait
                    repeat
                        event = v.events[v.event]
                        channel_id = event.channel
                        channel = channel_id and self.channels[channel_id + 1] or nil
                        event_wait = event_delta_time(event)
                        if v.clock >= event_wait then
                            v.clock = v.clock - event_wait

                            if event.type == "set tempo" then

                                -- SET TEMPO
                                self.tempo = event.tempo

                            elseif event.type == "note on" then

                                -- NOTE ON
                                local instrument = midi.get_instrument(channel.program, event.note)

                                local sound = Instance.new("Sound")
                                sound.SoundId = "rbxassetid://" .. instrument[1]
                                sound.Parent = script
                                sound.Pitch = 2 ^ ((event.note + instrument[2] - 60) / 12)
                                sound.Volume = (instrument[4] or 0.5) * event.velocity / 127 * self.volume
                                sound.Looped = instrument[8] and instrument[8] == 1 or false
                                sound.TimePosition = instrument[6] or 0
                                sound:Play()

                                --if instrument.last then
                                --    delay(sound.TimeLength, function()
                                --        sound:Stop()
                                --        sound:Destroy()
                                --    end)
                                --else
                                    sound:Play()
                                    channel.sounds[event.note] = sound
                                --end

                            elseif event.type == "note off" then

                                -- NOTE OFF
                                local instrument = midi.get_instrument(channel.program, event.note)
                                local sound = channel.sounds[event.note]
                                if sound--[[ and not in_range(channel.program + 1, 1, 8) and not in_range(channel.program + 1, 113, 120)]] then
                                    channel.sounds[event.note] = nil
                                    local w = 0.2
                                    if instrument[8] == 1 then
                                        w = 0
                                    elseif instrument[7] then
                                        w = instrument[7]
                                    end
                                    delay(w, function()
                                        sound:Stop()
                                        sound:Destroy()
                                    end)
                                else
                                    warn("attempt to note off on note " .. event.note .. ", channel " .. channel_id)
                                end

                            elseif event.type == "note aftertouch" then
                                
                                -- NOTE AFTERTOUCH
                                local instrument = midi.get_instrument(channel.program, event.note)
                                local sound = channel.sounds[event.note]
                                if sound then
                                    sound.Volume = (instrument[4] or 0.5) * event.aftertouch / 127 * self.volume
                                end

                            elseif event.type == "pitch bend" then

                                for key,sound in next, channel.sounds do
                                    local instrument = midi.get_instrument(channel.program, key)
                                    sound.Pitch = 2 ^ ((key + instrument[2] - 60 + event.pitch) / 12)
                                end
                            
                            elseif event.type == "program change" then

                                -- PROGRAM CHANGE
                                channel.program = event.program + 1
                            
                            end
                            
                            v.event = v.event + 1
                        end
                    until not v.events[v.event] or event_delta_time(v.events[v.event]) > 0.0000001
                end
            end
        end

        previous_time = time()
        heartbeat:wait()

        if self.stop then
            break
        end
    until #where(self.tracks, predicate.prop_eq("complete", true)) == #self.tracks or self.stop

    for i,v in next, self.channels do
        for _,sound in next, v.sounds do
            sound:Stop()
            sound:Destroy()
        end
    end

    midi.playing = false
end

-- Decodes the MIDI object.
midi.decode = function(self)
    --[[
        The header chunk is organized as:

        4-byte chunk ID -> "MThd"
        4-byte chunk size -> 6
        2-byte format type -> 0-2
        2-byte track count -> 1-65,536
        2-byte time division
    ]]--

    -- check header chunk ID
    local header_chunk_id = self.stream:next_string(4)
    assert(header_chunk_id == "MThd", "header chunk ID mismatch: expected 'MThd', got '" .. header_chunk_id .. "'")

    -- check header chunk size
    assert(self.stream:next_num(4 * 8) == 6, "header chunk size mismatch: expected 6")

    -- get format type
    self.format_type = self.stream:next_num(2 * 8)
    assert(in_range(self.format_type, 0, 2), "header format type mismatch: expected format in range 0-2")

    -- get number of tracks
    self.track_count = self.stream:next_num(2 * 8)
    assert(in_range(self.track_count, 1, 65535), "header track count mismatch: expected count in range 1-65536")

    -- get time division
    local time_division = self.stream:next_bits(2 * 8)
    
    do
        local division_type = time_division[1]

        if division_type == 0 then
            -- Time is in ticks per quarter note.
            self.ticks_per_quarter_note = num_from_bits(after(time_division, 2))
        else
            error("SMPTE timing is not currently supported")
        end
    end

    -- From here, we decode other types of chunks.
    while self.stream.char < #self.source do
        local chunk_id = self.stream:next_string(4)
        assert(contains({"MTrk"}, chunk_id), "chunk ID mismatch: expected valid chunk ID, got " .. chunk_id)

        local chunk_size = self.stream:next_num(32)
        self.stream.clock = 0

        if chunk_id == "MTrk" then
            -- This is a track chunk! Let's decode some events.
            local track = {
                events = {}
            }

            local last_known_type

            local last_channel_message

            while self.stream.clock < chunk_size * 8 do

                -- calculate the delta time
                local delta_time = self.stream:next_vlq()

                -- event type
                local event_type = self.stream:next_num(4)

                local event = {delta_time = delta_time}

                local function read_channel_message(ch)
                    local event_type = ch[1]
                    local channel = ch[2]

                    local function param() return self.stream:next_num(8) end
                    local function param7() self.stream:next_bit() return self.stream:next_bits(7) end

                    if event_type == 0x8 then
                        event.type = "note off"
                        event.note = param()
                        event.velocity = param()
                    elseif event_type == 0x9 then
                        event.type = "note on"
                        event.note = param()
                        event.velocity = param()
                    elseif event_type == 0xA then
                        event.type = "note aftertouch"
                        event.note = param()
                        event.aftertouch = param()
                    elseif event_type == 0xB then
                        event.type = "controller"
                        event.controller = param()
                        --print("control change: " .. event.controller .. ". last event: " .. track.events[#track.events].type)
                        if not in_range(event.controller, 120, 127) then event.value = param() end
                    elseif event_type == 0xC then
                        event.type = "program change"
                        event.program = param()
                    elseif event_type == 0xD then
                        event.type = "channel aftertouch"
                        event.value = param()
                    elseif event_type == 0xE then
                        event.type = "pitch bend"
                        local lsb, msb = param7(), param7()
                        event.pitch = (num_from_bits(merge(msb, lsb)) / math.pow(2, 14) - 0.5) * 4
                    end
                end

                if in_range(event_type, 0x8, 0xE) then
                    -- This is a MIDI event

                    -- channel
                    channel = self.stream:next_num(4)
                    event.channel = channel
                    event.event_type = "midi"

                    if in_range(event_type, 0x8, 0xE) then
                        last_channel_message = {event_type, channel}
                        read_channel_message(last_channel_message)
                    end

                elseif event_type == 0xF then
                    -- This is either a meta event or a system exclusive event
                    -- Meta events' following 4 bits should be an F, and a 0 for sysex events

                    local following = self.stream:next_num(4)

                    if following == 0xF then
                        -- Meta events

                        local meta_type = self.stream:next_num(8)

                        event.event_type = "meta"

                        -- meta_types from 0x01 to 0x0F are reserved for texts.
                        -- meta_type 0x7F is a sequencer-specific meta event. its length is a VLQ, so we can just pull out that many bytes

                        if in_range(meta_type, 0x01, 0x0F) then
                            local text_length = self.stream:next_vlq()
                            --print(text_length)
                            local text = self.stream:next_string(text_length)

                            local text_types = {
                                [0x01] = "text event",
                                [0x02] = "copyright notice",
                                [0x03] = "sequence/track name",
                                [0x04] = "instrument name",
                                [0x05] = "lyric",
                                [0x06] = "marker",
                                [0x07] = "cue point"
                            }

                            --assert(text_types[meta_type], "meta event text type mismatch: expected text type from 0x00 to 0x07, got " .. meta_type)

                            event.type = text_types[meta_type] or "unknown"
                            event.text = text
                        elseif meta_type == 0x7F then
                            -- Seq. specific meta event
                            self.stream:next_bytes(self.stream:next_vlq())
                        else
                            local params_length = self.stream:next_num(8)

                            if meta_type == 0x00 then
                                -- Sequence Number
                                assert(params_length == 2, "meta event 'sequence number' expects 2 params, got " .. params_length)

                                event.type = "sequence number"
                                event.msb = self.stream:next_num(8)
                                event.lsb = self.stream:next_num(8)
                            elseif meta_type == 0x20 then
                                -- MIDI Channel Prefix
                                assert(params_length == 1, "meta event 'channel prefix' expects 1 param, got " .. params_length)

                                event.type = "channel prefix"
                                event.channel = self.stream:next_num(8)
                            elseif meta_type == 0x21 then
                                -- MIDI port (safely disregard this event)
                                assert(params_length == 1, "meta event 'midi port' expects 1 param, got " .. params_length)
                                self.stream:next_bytes(1)
                            elseif meta_type == 0x2F then
                                -- End of Track
                                assert(params_length == 0, "meta event 'end of track' expects no params, got " .. params_length)

                                event.type = "end of track"
                            elseif meta_type == 0x51 then
                                -- Set Tempo
                                assert(params_length == 3, "meta event 'set tempo' expects 3 params, got " .. params_length)

                                event.type = "set tempo"
                                event.tempo = self.stream:next_num(3 * 8)
                            elseif meta_type == 0x54 then
                                -- SMPTE Offset
                                local length = params_length

                                event.type = "smpte offset"
                                if length >= 1 then event.hour = self.stream:next_num(8) end
                                if length >= 2 then event.minute = self.stream:next_num(8) end
                                if length >= 3 then event.second = self.stream:next_num(8) end
                                if length >= 4 then event.frame = self.stream:next_num(8) end
                                if length >= 5 then event.sub_frame = self.stream:next_num(8) end

                                event.hour = event.hour or 0
                                event.minte = event.minute or 0
                                event.second = event.second or 0
                                event.frame = event.frame or 0
                                event.sub_frame = event.sub_frame or 0

                                event.time_offset = event.hour * 3600 + event.minute * 60 + event.second + event.frame / 30 + event.sub_frame / 100

                                --assert(in_range(event.hour, 0, 23), "SMPTE offset hour must be in range 0-23, got " .. event.hour)
                                --assert(in_range(event.minute, 0, 59), "SMPTE offset minute must be in range 0-59, got " .. event.minute)
                                --assert(in_range(event.second, 0, 59), "SMPTE offset second must be in range 0-59, got " .. event.second)
                                --assert(in_range(event.frame, 0, 30), "SMPTE offset frame must be in range 0-30, got " .. event.frame)
                                --assert(in_range(event.sub_frame, 0, 99), "SMPTE offset sub-frame must be in range 0-99, got " .. event.sub_frame)
                            elseif meta_type == 0x58 then
                                -- Time Signature
                                assert(params_length == 4, "meta event 'time signature' expects 4 params, got " .. params_length)

                                event.type = "time signature"
                                event.numerator = self.stream:next_num(8)
                                event.denominator = math.pow(2, self.stream:next_num(8))
                                event.metronome = self.stream:next_num(8)
                                event.thirty_seconds = self.stream:next_num(8)
                            elseif meta_type == 0x59 then
                                -- Key Signature
                                assert(params_length == 2, "meta event 'key signature' expects 2 params, got " .. params_length)

                                event.type = "key signature"
                                event.offset = self.stream:next_num(8) - 128
                                event.major = self.stream:next_num(8) == 0
                            else
                                warn("meta event type mismatch: encountered unknown meta event type: " .. meta_type .. ". last known type was " .. (last_known_type or "?"))
                            end
                        end
                    
                    elseif following ~= 2 and following ~= 3 and following ~= 0 then
                        -- undefined/irrelevant
                        print 'asd irrelevant'
                    elseif following == 2 then
                        -- song position pointer
                        self.stream:next_bytes(2)
                        print'asd song position pointer'
                    elseif following == 3 then
                        -- song select
                        self.stream:next_bytes(1)
                        print'asd song select'
                    elseif following == 0x0 then
                        -- SysEx events
                        event.event_type = "sysex"
                        print("hit a sysex event. kinda garbage.")
                        local byte
                        repeat
                            byte = self.stream:next_num(8)
                        until byte == 247
                        print("sysex done.")
                    else
                        error("event type mismatch: expected 0xF or 0x0 matching 0xFx for event type, got " .. following)
                    end

                else
                    warn("event type mismatch: " .. event_type .. " is not a valid event type. treating as running status. last known type was " .. (last_known_type or "?"))
                    --self.stream:next_bits(4 + 8 + 8)
                    event.event_type = "midi"
                    event.channel = last_channel_message[2]
                    read_channel_message(last_channel_message)
                end

                if event.type then last_known_type = event.type end

                table.insert(track.events, event)

            end

            table.insert(self.tracks, track)

        end
    end

    return self
end

--------------------------------------------------------------------

andromeda = {}
andromeda.config = {
    prefix = "\\",

    players = {
        {id = 6996776, rank = 3},  --voximity

        {id = 10817923, rank = 3}, --rytone
        {id = 47480314, rank = 2}, --insert_username04
        {id = 1412979, rank = 2},  --Cooldude341
        {id = 189503, rank = 2},   --Reinitialized
        {id = 17851297, rank = 2}, --jarredbcv
        {id = 2199508, rank = 2},  --AmbientOcclusion
    },

    ranks = {
        {name = "Admin", color = "Dark Royal blue", id = 3},
        {name = "Operator", color = "Gold", id = 2},
        {name = "Moderator", color = "Faded green", id = 1},
        {name = "User", color = "Transparent", id = 0},
        {name = "Banned", color = "Gun metallic", id = -1}
    },

    sounds = {
        new_group = 131208997
    }
}

--------------------------------------------------------------------

rank = {}

-- Gets the rank metadata given its id.
rank.get = function(i)
    return find(andromeda.config.ranks, predicate.prop_eq("id", i))
end

-- Gets a player's config rank.
rank.player = function(p)
    local o = find(andromeda.config.players, predicate.prop_eq("id", p.UserId))
    if o then return o end

    o = {id = p.UserId, rank = 0}
    table.insert(andromeda.config.players, o)
    return o
end

-- Sets a player's rank.
rank.set = function(p, r)
    if rank.player(p).rank == r then return end

    rank.player(p).rank = r
    if r == -1 then
        p:Kick("You have been banned by an Andromeda administrator.")
    else
        tablet.add(p, "You are now Rank " .. r .. ".")
    end
end

-- Formats a rank as 1, 2 (where 1 is the rank num and 2 is the rank name)
rank.format = function(x)
    if type(x) == "number" then
        return rank.get(x).id .. ", " .. rank.get(x).name
    else
        return x.id .. ", " .. x.name
    end
end

--------------------------------------------------------------------

tablet = {rotation_rate = 0.004}

tablet.common = {
    dismiss_self = function(self) self:dismiss() end,
    dismiss_group = function(self) self.group:dismiss() end
}

tablet.groups = {}

tablet.misc = function(player)
    local group = find(tablet.groups, predicate.both(predicate.prop_eq("player", player), predicate.prop_eq("misc", true)))
    if group then return group end

    return tablet_group.new(player, true)
end

tablet.add = function(player, data)
    tablet.misc(player):add_tablet(type(data) == "string" and {text = data, clicked = tablet.common.dismiss_self} or data)
end

tablet.warn = function(player, text)
    tablet.add(player, {text = text, color = "Crimson", clicked = tablet.common.dismiss_self})
end

tablet_group = {}
tablet_group.meta = {__index = tablet_group}

tablet_group.new = function(player, misc)
    local group = {}

    -- play the iconic sound
    if not misc and exists(player.Character) then play_sound(andromeda.config.sounds.new_group, player.Character.HumanoidRootPart) end

    -- the player that this group is bound to
    group.player = player

    -- the rotation of the tablets in this group (necessary for pausing rotation on hover)
    group.rotation = 0

    -- the rate at which rotation increases when no tablets are being hovered over
    group.rotation_rate = tablet.rotation_rate

    -- the list of tablets in the group
    group.list = {}

    -- whether or not this group is a miscellaneous group, e.g. it never disappears + random tablets are thrown into it
    group.misc = misc or false

    setmetatable(group, tablet_group.meta)
    table.insert(tablet.groups, group)

    return group
end

tablet_group.dismiss = function(self, keep)
    each_reverse(self.list, function(t, i)
        t:dismiss(keep)
    end)
end

tablet_group.add_dismiss = function(self)
    self:add_tablet {text = "Dismiss", color = "Bright red", clicked = tablet.common.dismiss_group}
end

tablet_group.add_back = function(self, func)
    self:add_tablet {text = "Back", color = "Really blue", clicked = func}
end

tablet_group.add_tablet = function(self, meta)
    meta.color = meta.color or rank.get(rank.player(self.player).rank).color

    local t = {
        player = self.player,
        color = meta.color,
        text = meta.text,
        clicked = meta.clicked,
        angles = {tablet.random_angle_pair()},
        group = self,

        hovering = false,
        dismiss = function(self, keep)
            remove_where(self.group.list, predicate.eq(self))
            if #self.group.list == 0 and not self.group.misc and not keep then
                remove_where(tablet.groups, predicate.eq(self.group))
            end

            -- TODO: animate dismissal
            self.part:Destroy()
        end
    }

    t.update = function(self)
        local t = self
        local part = t.part

        part.Color = type(t.color) == "string" and BrickColor.new(t.color).Color or (typeof(t.color) == "BrickColor" and t.color.Color or t.color)
        part.BillboardGui.TextLabel.TextStrokeColor3 = Color3.new(math.min(part.Color.r, 0.6), math.min(part.Color.g, 0.6), math.min(part.Color.b, 0.6))
        part.BillboardGui.TextLabel.Text = t.text or ""
    end

    do -- part instantiation

        local part = Instance.new("Part")
        part.Size = Vector3.new(1.2, 1.2, 1.2)
        part.Anchored = true
        part.CanCollide = false
        part.TopSurface = "Smooth"
        part.BottomSurface = "Smooth"
        part.Transparency = 0.5
        if exists(t.player.Character) then part.CFrame = t.player.Character.HumanoidRootPart.CFrame * CFrame.new(0, -5, 0) end
        part.Parent = script

        local box = Instance.new("SelectionBox")
        box.LineThickness = 0.02
        box.Color3 = Color3.new(1, 1, 1)--part.Color
        box.Transparency = 0.9
        box.Adornee = part
        box.Parent = part

        local gui = Instance.new("BillboardGui")
        gui.Size = UDim2.new(0, 100, 0, 100)
        gui.StudsOffset = Vector3.new(0, 1.5, 0)
        gui.MaxDistance = 50
        gui.Adornee = part
        gui.Parent = part

        local label = Instance.new("TextLabel")
        label.Font = "SourceSans"
        label.TextSize = 20
        label.BackgroundTransparency = 1
        label.TextColor3 = Color3.new(1, 1, 1)
        label.TextStrokeTransparency = 0.6
        label.Size = UDim2.new(1, 0, 1, 0)
        label.Parent = gui

        if t.clicked then
            local detector = Instance.new("ClickDetector")
            detector.MaxActivationDistance = gui.MaxDistance

            local function can_interact(player)
                return player == t.player or rank.player(player).rank > rank.player(t.player).rank or rank.player(player).rank == 3
            end

            detector.MouseClick:connect(function(player)
                if can_interact(player) then
                    t.clicked(t, player)
                end
            end)

            detector.MouseHoverEnter:connect(function(player)
                if can_interact(player) then
                    t.hovering = true
                end
            end)

            detector.MouseHoverLeave:connect(function(player)
                if can_interact(player) then
                    t.hovering = false
                end
            end)

            detector.Parent = part
        end

        t.part = part
        t:update()
    end

    table.insert(self.list, t)
end

tablet.random_angle_pair = function()
    local rand = function(n) return math.random(-(n or 0), n or math.pi * 2) end
    local arx, ary, arz = rand(), rand(), rand()
    local brx, bry, brz = arx + 0.6, ary + 0.6, arz + 0.6--arx + rand(1), ary + rand(1), arz + rand(1)
    return CFrame.Angles(arx, ary, arz), CFrame.Angles(brx, bry, brz)
end

tablet.bounce = 0
tablet.update = function()
    tablet.bounce = tablet.bounce + 0.004
    -- iterate over each player
    each(service("Players"):GetPlayers(), function(player)
        local char = player.Character

        local groups = where(tablet.groups, function(group) return group.player == player and #group.list > 0 end)
        table.sort(groups, function(a, b) return #a.list < #b.list end)
        local r = 1.5

        each(groups, function(group, group_index)
            r = r + math.max(2, (#group.list - 1) / r * 0.4)

            local hovering = any(group.list, predicate.prop_eq("hovering", true))
            group.rotation_rate = lerp(group.rotation_rate, hovering and 0 or tablet.rotation_rate, 0.1)
            group.rotation = group.rotation + group.rotation_rate

            each(group.list, function(t, i)

                local bounce = (tablet.bounce + i / #group.list) * math.pi * 2
                local angular_position = (i / #group.list) * math.pi * 2 + group.rotation

                if exists(char) and char:FindFirstChild("HumanoidRootPart") then
                    local root = char.HumanoidRootPart.Position
                    local pos = root + Vector3.new(math.cos(angular_position) * r, math.cos(bounce) * 0.25, math.sin(angular_position) * r)

                    t.part.CFrame = CFrame.new(t.part.Position:Lerp(pos, 0.3)) * t.angles[1]:lerp(t.angles[2], math.cos(bounce) * 0.5 + 0.5)
                end

                -- HOVERING
                local label = t.part.BillboardGui.TextLabel
                local box = t.part.SelectionBox
                label.ZIndex = 1

                if hovering and not t.hovering then
                    label.TextTransparency = lerp(label.TextTransparency, 0.7, 0.1)
                    label.TextStrokeTransparency = lerp(label.TextStrokeTransparency, 0.9, 0.1)
                else
                    label.TextTransparency = lerp(label.TextTransparency, 0, 0.1)
                    label.TextStrokeTransparency = lerp(label.TextStrokeTransparency, 0.6, 0.1)
                end

                box.LineThickness = lerp(box.LineThickness, 0.02, 0.1)
                box.Transparency = lerp(box.Transparency, 0.9, 0.1)

                if t.hovering then
                    box.LineThickness = lerp(box.LineThickness, 0.06, 0.1)
                    box.Transparency = lerp(box.Transparency, 0, 0.1)
                    label.ZIndex = 2
                end

            end)
        end)
    end)
end

--------------------------------------------------------------------

local shield = {}
shield.list = {}

shield.new = function(player, radius)
    local s = {}

    s.player = player
    s.radius = radius or 4.5
    s.detail = 16
    s.offset = math.random(0, math.pi * 2)

    setmetatable(s, {__index = shield})
    s:hook()

    table.insert(shield.list, s)

    return s
end

shield.hook = function(self)
    if self.player.Character and self.player.Character:FindFirstChild("HumanoidRootPart") then
        self:create()
    end

    self.character_hook = self.player.CharacterAdded:connect(function(char)
        char:WaitForChild("HumanoidRootPart")
        self:create()
    end)
end

shield.dispose = function(self)
    self.character_hook:disconnect()

    if exists(self.model) then
        self.model:Destroy()
        self.model = nil
    end

    remove_where(shield.list, predicate.eq(self))
end

shield.create = function(self)
    if not self.player.Character then return end
    local s = self

    s.model = Instance.new("Model")
    s.model.Name = "Shield"

    s.part = Instance.new("Part")
    s.part.Shape = "Ball"
    s.part.Transparency = 0.5
    s.part.BrickColor = BrickColor.new(rank.get(rank.player(self.player).rank).color)
    s.part.Material = "ForceField"
    s.part.CanCollide = false
    s.part.Anchored = true
    s.part.CastShadow = false
    s.part.Size = Vector3.new(self.radius * 2, self.radius * 2, self.radius * 2)
    s.part.Parent = s.model

    s.details = {}

    for i = 1, s.detail do
        local p = Instance.new("Part")
        p.TopSurface = "Smooth"
        p.BottomSurface = "Smooth"
        p.Material = "Neon"
        p.BrickColor = BrickColor.new("Medium stone grey")
        p.CanCollide = false
        p.Anchored = true
        p.CastShadow = false
        p.Parent = s.model

        table.insert(s.details, p)
    end

    s.model.Parent = self.player.Character
end

shield.update = function()
    each(shield.list, function(s)
        if not exists(s.player) then s:dispose() end
        if not exists(s.player.Character) or not exists(s.player.Character.Humanoid) then s.model:Destroy() s.model = nil end
        if exists(s.player.Character) and not s.model then s:create() end

        if s.model then
            local cf = CFrame.new(s.player.Character:FindFirstChild("HumanoidRootPart").Position)
            s.part.CFrame = cf

            each(s.details, function(d, i)
                local bounce = tablet.bounce + s.offset

                local radius = math.abs(math.cos(bounce * 4) * s.radius)
                local length = 2 * math.pi * radius / s.detail
                local angle = i / s.detail * math.pi * 2
                d.Size = Vector3.new(length, 0.2, 0.2)
                d.CFrame = cf * CFrame.Angles(bounce * 2, bounce * 1.47, bounce * 0.73) * CFrame.new(math.cos(angle) * radius, math.sin(bounce * 4) * s.radius, math.sin(angle) * radius) * CFrame.Angles(0, -angle + math.pi / 2, 0)
            end)

            each(where(service("Players"):GetPlayers(), predicate.both(predicate.neq(s.player), function(x) return not any(shield.list, predicate.prop_eq("player", x)) end)), function(p)
                if not exists(p.Character) or not p.Character:FindFirstChild("HumanoidRootPart") or not exists(p.Character.Humanoid) or not p.Character.Humanoid.Health == 0 then return end
                local dist = (p.Character.HumanoidRootPart.Position - s.part.CFrame.p).magnitude

                if dist <= s.radius + 1 then
                    p.Character:BreakJoints()
                end
            end)
        end
    end)
end

--------------------------------------------------------------------

parse_argument = function(player, arg, raw_arg)
    if arg.type == "string" then
        return true, raw_arg
    elseif arg.type == "selection" then
        if contains(arg.selections, raw_arg:lower()) then
            return true, raw_arg:lower()
        else
            return false, "Value must be one of [" .. table.concat(arg.selections, ", ") .. "]."
        end
    elseif arg.type == "number" then
        local n = tonumber(raw_arg)
        if n == nil then return false, nil end

        if arg.upper_bound and n > arg.upper_bound then return false, "Value is out of bounds." end
        if arg.lower_bound and n < arg.lower_bound then return false, "Value is out of bounds." end

        return true, n
    elseif arg.type == "players" then
        return true, match_players(player, raw_arg)
    elseif arg.type == "player" then
        return true, match_players(player, raw_arg)[1] or nil
    end
end

commands = {


    {
        aliases = {"eval", "evaluate", "loadstring"},
        name = "Evaluate",
        desc = "Evaluates the given string.",
        args = {},
        rank = 3,
        func = function(self, ctx)
            if string_index_of(ctx.message, " ") == -1 then return end

            local load_func, load_err = loadstring(ctx.message:sub(string_index_of(ctx.message, " ")))
            if not load_func then
                tablet.warn(ctx.player, "Failed to interpret." .. (load_err and "\n" .. load_err or ""))
                return
            end

            local s, e = pcall(load_func)
            if not s then
                tablet.warn(ctx.player, "Failed to evaluate." .. (e and "\n" .. e or ""))
                return
            end

            if e then
                tablet.add(ctx.player, {color = "Bright green", text = tostring(e), clicked = tablet.common.dismiss_self})
            end
        end
    },


    {
        aliases = {"dismiss", "dt"},
        name = "Dismiss",
        desc = "Dismisses the player's active tablet groups.",
        args = {},
        rank = 0,
        func = function(self, ctx)
            each_reverse(where(tablet.groups, predicate.prop_eq("player", ctx.player)), function(group) group:dismiss() end)
        end
    },


    {
        aliases = {"dismissplayer", "dtp", "dismissp"},
        name = "Dismiss Player",
        desc = "Dismisses another player's tablets.",
        args = {{name = "players", type = "players", required = true}},
        rank = 2,
        func = function(self, ctx)
            each(ctx.args.players, function(player)
                command("dismiss"):func {player = player}
            end)
        end
    },


    {
        aliases = {"shield", "sh"},
        name = "Shield",
        desc = "Shields the player from nearby players.",
        args = {},
        flags = {
            {name = "radius", type = "number", upper_bound = 15, lower_bound = 3.5, aliases = {"radius", "r"}}
        },
        rank = 1,
        func = function(self, ctx)
            local s = find(shield.list, predicate.prop_eq("player", ctx.player))
            if s then
                s:dispose()
                return
            end

            s = shield.new(ctx.player, ctx.flags.radius)
        end
    },


    {
        aliases = {"ping", "p"},
        name = "Ping",
        desc = "Creates a tablet with the supplied text.",
        args = {},
        rank = 0,
        flags = {{name = "count", type = "number", upper_bound = 100, lower_bound = 1, aliases = {"count", "c"}}},
        func = function(self, ctx)
            local texts = #ctx.raw_args == 0 and {"Pong!"} or ctx.raw_args
            local ind = 0
            local n = #texts * (ctx.flags.count or 1)

            for i = 1, ctx.flags.count or 1 do
                ind = ind + 1
                each(texts, function(text)
                    local col = BrickColor.Random()
                    local r, g, b
                    if n > 1 then
                        r, g, b = hsv_to_rgb((ind - 1) / n, 1, 1)
                        col = Color3.new(r / 255, g / 255, b / 255)
                    end

                    tablet.misc(ctx.player):add_tablet {text = text, color = col, clicked = tablet.common.dismiss_self}
                end)
            end
        end
    },


    {
        aliases = {"kill"},
        name = "Kill",
        desc = "Kills a player.",
        rank = 1,
        args = {{name = "victims", type = "players", required = true}},
        flags = {
            {name = "explode", type = "flag", aliases = {"explode", "e"}},
            {name = "load", type = "flag", aliases = {"load", "l"}},
            {name = "reload", type = "flag", aliases = {"reload", "r"}}
        },
        func = function(self, ctx)
            each(ctx.args.victims, function(player)
                if ctx.flags.load or (not exists(player.Character) and ctx.flags.reload) then
                    player:LoadCharacter()
                    return
                end

                if exists(player.Character) then
                    if ctx.flags.explode then
                        Instance.new("Explosion", workspace).Position = player.Character.HumanoidRootPart.Position
                        return
                    end

                    if ctx.flags.reload then
                        local cf = player.Character.HumanoidRootPart.CFrame
                        player:LoadCharacter()
                        player.Character:WaitForChild("HumanoidRootPart").CFrame = cf
                        return
                    end

                    player.Character:BreakJoints()
                end
            end)
        end
    },


    {
        aliases = {"kick"},
        name = "Kick",
        desc = "Kicks a player.",
        rank = 2,
        args = {
            {name = "victims", type = "players", required = true}
        },
        flags = {
            {name = "reason", type = "string", aliases = {"reason"}}
        },
        func = function(self, ctx)
            each(ctx.args.victims, function(player)
                player:Kick("You have been kicked from the game by an Andromeda " .. rank.get(rank.player(ctx.player).rank).name .. "." .. (ctx.flags.reason and "\n" .. ctx.flags.reason or ""))
            end)
        end
    },


    {
        aliases = {"setrank", "sr"},
        name = "Set Rank",
        desc = "Sets a group of players' ranks.",
        rank = 3,
        args = {
            {name = "players", type = "players", required = true},
            {name = "rank", type = "number", required = true}
        },
        func = function(self, ctx)
            if ctx.args.rank > 3 or ctx.args.rank < -1 then
                tablet.warn(ctx.player, "That rank does not exist.")
                return
            end

            if #ctx.args.players == 0 then
                tablet.warn(ctx.player, "No valid players were passed.")
                return
            end

            each(ctx.args.players, function(p)
                if p == ctx.player then
                    tablet.warn(p, "You cannot set your own rank.")
                    return
                end

                rank.set(p, ctx.args.rank)
            end)
        end
    },


    {
        aliases = {"playsound", "sound"},
        name = "Play Sound",
        desc = "Plays a sound.",
        rank = 1,
        args = {{name = "id", type = "number", required = true}},
        flags = {
            {name = "global", type = "flag", aliases = {"global", "g"}}
        },
        func = function(self, ctx)
            local parent = ctx.flags.global and workspace or (exists(ctx.player.Character) and ctx.player.Character.HumanoidRootPart or workspace)
            play_sound(ctx.args.id, p)
        end
    },


    {
        aliases = {"sudo"},
        name = "Sudo",
        desc = "Forces another player to execute a command.",
        rank = 3,
        join_last_argument = false,
        args = {
            {name = "players", type = "players", required = true},
            {name = "command", type = "string", required = true}
        },
        flags = {
            {name = "ignore_rank", type = "flag", aliases = {"ignore-rank", "i"}},
            {name = "count", type = "number", upper_bound = 500, lower_bound = 1, aliases = {"count", "repeat", "c"}}
        },
        func = function(self, ctx)
            local cmd = command(ctx.args.command)
            if not cmd then
                tablet.warn(ctx.player, "No command named \"" .. ctx.args.command .. "\".")
                return
            end

            each(ctx.args.players, function(p)
                for i = 1, ctx.flags.count or 1 do
                    local m = andromeda.config.prefix .. "\"" .. ctx.args.command .. "\" " .. join_args(after(ctx.raw_args, 3))
                    parse_chat(p, m, ctx.flags.ignore_rank)
                end
            end)
        end
    },


    {
        aliases = {"tp", "teleport"},
        name = "Teleport",
        desc = "Teleports player(s) to another. An intermediate method must be passed.",
        rank = 1,
        args = {
            {name = "players", type = "players", required = true},
            {name = "method", type = "selection", selections = {"to", "atop", "on", "around", "near"}, required = true},
            {name = "target", type = "player", required = true}
        },
        flags = {},
        func = function(self, ctx)
            if not exists(ctx.args.target.Character) or not ctx.args.target.Character:FindFirstChild("HumanoidRootPart") then
                tablet.warn(ctx.player, "The target player's character is presently nonexistent.")
                return
            end

            local target_root = ctx.args.target.Character.HumanoidRootPart

            each(ctx.args.players, function(player, i)
                if not exists(player.Character) then return end
                local root = player.Character:FindFirstChild("HumanoidRootPart")
                if not root then return end

                local cf
                if ctx.args.method == "to" then
                    cf = target_root.CFrame
                elseif ctx.args.method == "atop" or ctx.args.method == "on" then
                    cf = target_root.CFrame * CFrame.new(0, 5 * i, 0)
                elseif ctx.args.method == "around" then
                    cf = CFrame.new(target_root.Position + Vector3.new(math.cos((i - 1) / #ctx.args.players * math.pi * 2) * 4, 0, math.sin((i - 1) / #ctx.args.players * math.pi * 2) * 4), target_root.Position)
                elseif ctx.args.method == "near" then
                    --idek what to do with this yet...
                    cf = target_root.CFrame
                end

                root.CFrame = cf
            end)
        end
    },


    {
        aliases = {"midi"},
        name = "MIDI Player",
        desc = "Provides control to the internal MIDI player.",
        rank = 1,
        args = {},
        flags = {
            {name = "play", type = "string", aliases = {"play", "p"}},
            {name = "skip", type = "flag", aliases = {"skip", "s"}},
            {name = "stop", type = "flag", aliases = {"stop"}},
            {name = "volume", type = "number", lower_bound = 0, upper_bound = 1, aliases = {"volume", "vol", "v"}},
            {name = "tempo", type = "number", lower_bound = 0.1, upper_bound = 10, aliases = {"tempo", "speed", 't'}}
        },

        queue = {},

        play_queue = function(self)
            local source_url = table.remove(self.queue, 1)
            local s, e = pcall(function()
                local source = service("HttpService"):GetAsync(source_url.url)
                local mid = midi.new(source)

                print("Decoding MIDI from URL " .. source_url.url .. "...")
                mid:decode()
                print("Beginning playback of MIDI...")
                mid:play()
                print("MIDI playback finished.")

                if #self.queue > 0 then
                    self:play_queue()
                end
            end)

            if not s then
                print("MIDI playback failed.\n\n" .. e)
                tablet.warn(source_url.player, "An error occured while decoding/playing your MIDI.")
                if #self.queue > 0 then
                    self:play_queue()
                end
            end
        end,

        func = function(self, ctx)
            if ctx.flags.skip then

                if midi.playing then
                    midi.playing.stop = true
                else
                    tablet.warn(ctx.player, "No MIDI sequence is currently playing.")
                end

            end

            if ctx.flags.stop then

                self.queue = {}
                if midi.playing then
                    midi.playing.stop = true
                    midi.playing = nil
                else
                    tablet.warn(ctx.player, "No MIDI sequence is currently playing.")
                end

            end

            if ctx.flags.play then

                table.insert(self.queue, {url = ctx.flags.play, player = ctx.player})
                if midi.playing then
                    tablet.add(ctx.player, "The MIDI file has been added into the queue at position " .. #self.queue .. ".")
                    return
                end
                spawn(function() self:play_queue() end)
                wait(1)

            end

            if ctx.flags.volume then

                if midi.playing then
                    midi.playing.volume = ctx.flags.volume
                else
                    tablet.warn(ctx.player, "No MIDI sequence is playing.")
                end
            
            end

            if ctx.flags.tempo then

                if midi.playing then
                    midi.tempo_multiplier = ctx.flags.tempo
                else
                    tablet.warn(ctx.player, "No MIDI sequence is playing.")
                end

            end

            if not empty(ctx.flags) then return end

            local group = tablet_group.new(ctx.player)

            group:add_tablet {text = (midi.playing and "MIDI is playing" or "No MIDI is playing"), color = (midi.playing and "Bright green" or "Bright red")}
            group:add_tablet {text = "Queue has " .. #self.queue .. " items"}
            group:add_tablet {text = "Skip", color = "Bright red", clicked = function()
                group:dismiss()
                self:func {player = ctx.player, flags = {skip = true}}
            end}

            group:add_tablet {text = "Stop", color = "Bright red", clicked = function()
                group:dismiss()
                self:func {player = ctx.player, flags = {stop = true}}
            end}

            group:add_dismiss()
        end
    },


    {
        aliases = {"whitelist"},
        name = "Whitelist",
        desc = "Allows management of Andromeda's server whitelist.",
        rank = 2,
        args = {},
        flags = {
            {name = "add", type = "string", aliases = {"add", "allow", "a"}},
            {name = "add_all", type = "flag", aliases = {"add-all", "allow-all"}},

            {name = "reset", type = "flag", aliases = {"reset"}},

            {name = "enable", type = "flag", aliases = {"enable", "e"}},
            {name = "disable", type = "flag", aliases = {"disable", "d"}}
        },

        whitelist = {},
        whitelist_enabled = false,

        setup_whitelist = function(self)
            self.whitelist = {}

            each(service("Players"):GetPlayers(), function(player)
                if rank.player(player).rank > 0 then
                    table.insert(self.whitelist, player.UserId)
                end
            end)
        end,

        whitelisted = function(self, player)
            if contains(self.whitelist, player.UserId) then return true end
            if rank.player(player).rank > 0 then
                table.insert(self.whitelist, player.UserId)
                return true
            end

            return false
        end,

        check_player = function(self, player)
            if not self.whitelist_enabled then return true end

            if not self:whitelisted(player) then
                player:Kick("You are not whitelisted in this server.")
                return false
            end

            return true
        end,

        enable_whitelist = function(self)
            self.whitelist_enabled = true

            each(service("Players"):GetPlayers(), function(player)
                self:check_player(player)
            end)
        end,

        disable_whitelist = function(self)
            self.whitelist_enabled = false
        end,

        func = function(self, ctx)
            if ctx.flags.reset then
                self:disable_whitelist()
                self:setup_whitelist()
            end

            if ctx.flags.add_all then
                each(service("Players"):GetPlayers(), function(p)
                    if not self:whitelisted(p) then
                        table.insert(self.whitelist, p.UserId)
                    end
                end)
            end

            if ctx.flags.add then
                local target = ctx.flags.add
                if not tonumber(target) then target = service("Players"):GetUserIdFromNameAsync(target) end

                local name = service("Players"):GetNameFromUserIdAsync(target)
                if contains(self.whitelist, target) then
                    tablet.warn(ctx.player, name .. " is already whitelisted.")
                else
                    table.insert(self.whitelist, target)
                    tablet.add(ctx.player, "Whitelisted " .. name .. ".")
                end
            end

            if ctx.flags.enable then
                self:enable_whitelist()
            end

            if ctx.flags.disable then
                self:disable_whitelist()
            end

            if #self.whitelist == 0 then self:setup_whitelist() end

            local group = tablet_group.new(ctx.player)

            group:add_tablet {text = "Whitelist " .. (self.whitelist_enabled and "enabled" or "disabled"), color = self.whitelist_enabled and "Bright green" or "Bright red", clicked = function(t)
                if not contains(self.whitelist, ctx.player.UserId) then tablet.warn(ctx.player, "You are not whitelisted, so you cannot enable/disable the whitelist.") return end
                
                if self.whitelist_enabled then
                    self:disable_whitelist()
                else
                    self:enable_whitelist()
                end

                t.text = "Whitelist " .. (self.whitelist_enabled and "enabled" or "disabled")
                t.color = self.whitelist_enabled and "Bright green" or "Bright red"
                t:update()
            end}

            each(unique(merge(self.whitelist, map(service("Players"):GetPlayers(), function(p) return p.UserId end))), function(uid)
                local player = find(service("Players"):GetPlayers(), predicate.prop_eq("UserId", uid))
                local name = player and player.Name or service("Players"):GetNameFromUserIdAsync(uid)
                local whitelisted = player and self:whitelisted(player) or contains(self.whitelist, uid)

                group:add_tablet {
                    text = name .. (not player and " [offline]" or "") .. "\n" .. (whitelisted and "Whitelisted" or "Not whitelisted"),
                    color = whitelisted and "Bright green" or "Bright red",
                    clicked = function(t)
                        if not player and whitelisted then
                            t:dismiss()
                            remove_where(self.whitelist, predicate.eq(uid))
                            return
                        end

                        if whitelisted then
                            remove_where(self.whitelist, predicate.eq(uid))
                            whitelisted = false
                        else
                            table.insert(self.whitelist, uid)
                            whitelisted = true
                        end

                        t.color = whitelisted and "Bright green" or "Bright red"
                        t.text = name .. "\n" .. (whitelisted and "Whitelisted" or "Not whitelisted")
                        t:update()
                    end
                }
            end)

            group:add_dismiss()
        end
    },


    {
        aliases = {"cmds", "commands", "help"},
        name = "Commands",
        desc = "Shows a list of commands.",
        rank = 0,

        display_command = function(self, ctx, cmd, return_to)
            local group = ctx.group
            group:dismiss(true)

            local color = rank.get(cmd.rank).color

            group:add_tablet {text = "Name:\n" .. cmd.name, color = color}
            group:add_tablet {text = "Aliases:\n" .. table.concat(map(cmd.aliases, function(x) return andromeda.config.prefix .. x end), ", "), color = color}
            group:add_tablet {text = "Description:\n" .. cmd.desc, color = color}
            group:add_tablet {text = "Rank required:\n" .. rank.format(cmd.rank), color = color}
            group:add_tablet {text = (not cmd.args or #cmd.args == 0) and "No arguments" or ("Arguments:\n" .. table.concat(map(cmd.args or {}, function(x) return x.name .. " (" .. x.type .. (not x.required and ", optional" or "") .. ")" end), "\n")), color = color}

            if cmd.flags and #cmd.flags > 0 then
                group:add_tablet {
                    text = "Flags:\n" .. table.concat(map(cmd.flags, function(f) return table.concat(map(f.aliases, function(a) return #a == 1 and "-" .. a or "--" .. a end), "/") .. (f.type ~= "flag" and " <" .. f.type .. ">" or "") end), "\n"),
                    color = color
                }
            end

            if not any(cmd.args or {}, predicate.prop_eq("required", true)) and rank.player(ctx.player).rank >= cmd.rank then
                group:add_tablet {text = "Run Command", color = color, clicked = function()
                    cmd:func {
                        player = ctx.player,
                        command = cmd,
                        args = {},
                        flags = {},
                        raw_args = {}
                    }
                end}
            end

            if ctx.command == self and return_to then group:add_back(function() self:display_commands(ctx, return_to) end) end
            group:add_dismiss()
        end,

        display_commands = function(self, ctx, cmds)
            local group = ctx.group
            group:dismiss(true)
            table.sort(cmds, function(a, b) return a.rank < b.rank end)

            each(cmds, function(cmd)
                group:add_tablet {text = cmd.name, color = rank.get(cmd.rank).color, clicked = function()
                    self:display_command(ctx, cmd, cmds)
                end}
            end)

            if ctx.command == self then group:add_back(function() self:main_menu(ctx) end) end
            group:add_dismiss()
        end,

        main_menu = function(self, ctx)
            local group = ctx.group
            group:dismiss(true)

            group:add_tablet {text = "Your rank is\n" .. rank.format(rank.player(ctx.player).rank)}

            each_reverse(andromeda.config.ranks, function(r)
                group:add_tablet {text = "View " .. rank.format(r) .. " commands", color = r.color, clicked = function()
                    self:display_commands(ctx, where(commands, predicate.prop_eq("rank", r.id)))
                end}
            end)

            group:add_tablet {text = "View all commands", color = "Institutional white", clicked = function()
                self:display_commands(ctx, commands)
            end}

            group:add_dismiss()
        end,

        func = function(self, ctx)
            ctx.group = tablet_group.new(ctx.player)
            self:main_menu(ctx)
        end
    },


    {
        aliases = {"players", "playerlist", "list"},
        name = "Player List",
        desc = "Shows the player a list of players.",
        rank = 0,

        calculate_account_age = function(self, player)
            local total_days = player.AccountAge
            return math.floor(total_days / 365), total_days % 365
        end,

        set_rank = function(self, ctx, player)
            local group = ctx.group

            group:dismiss(true)

            each(andromeda.config.ranks, function(r)
                group:add_tablet {
                    text = "Set " .. player.Name .. "'s rank to\n" .. r.id .. ", " .. r.name,
                    color = r.color,
                    clicked = function()
                        rank.set(player, r.id)
                        if ctx.command == self then
                            self:view_player(ctx, player)
                        else
                            group:dismiss()
                        end
                    end
                }
            end)

            group:add_dismiss()
        end,

        ranked_command_tablet = function(self, ctx, text, command, args, flags)
            local r = rank.player(ctx.player).rank
            if r < command.rank then return end

            ctx.group:add_tablet {
                text = text,
                color = rank.get(command.rank).color,
                clicked = function()
                    command:func {
                        player = ctx.player,
                        args = args or {},
                        flags = flags or {},
                        command = command
                    }
                end
            }
        end,

        view_player = function(self, ctx, player)
            local group = ctx.group

            group:dismiss(true)
            local player_rank = rank.get(rank.player(player).rank)
            local color = player_rank.color
            local age = {self:calculate_account_age(player)}

            group:add_tablet {text = "Name:\n" .. player.Name, color = color}
            group:add_tablet {text = "User ID:\n" .. player.UserId, color = color}
            group:add_tablet {text = "Account Age:\n" .. age[1] .. " years, " .. age[2] .. " days", color = color}
            group:add_tablet {text = "Rank:\n" .. player_rank.id .. ", " .. player_rank.name, color = color}

            -- rank 1 commands
            self:ranked_command_tablet(ctx, "Kill Player", command("kill"), {victims = {player}})

            -- rank 2 commands
            self:ranked_command_tablet(ctx, "Kick Player", command("kick"), {victims = {player}})
            self:ranked_command_tablet(ctx, "Dismiss Player's Tablets", command("dtp"), {players = {player}})

            -- rank 3 commands

            -- set rank tablet (special condition)
            if rank.player(ctx.player).rank >= 3 then
                group:add_tablet {text = "Set Rank", color = rank.get(3).color, clicked = function() self:set_rank(ctx, player) end}
            end

            if ctx.command == self then group:add_back(function() self:view_list(ctx) end) end
            group:add_dismiss()
        end,

        view_list = function(self, ctx)
            ctx.group:dismiss(true)
            local players = service("Players"):GetPlayers()
            table.sort(players, function(a, b) return rank.player(a).rank < rank.player(b).rank end)

            each(players, function(player)
                ctx.group:add_tablet {
                    text = player.Name,
                    color = rank.get(rank.player(player).rank).color,
                    clicked = function()
                        self:view_player(ctx, player)
                    end
                }
            end)

            ctx.group:add_dismiss()
        end,

        func = function(self, ctx)
            ctx.group = tablet_group.new(ctx.player)
            self:view_list(ctx)
        end
    }
}

command = function(alias)
    return find(commands, predicate.prop_contains("aliases", alias:lower()))
end

parse_chat = function(player, message, ignore_rank)
    local prefix = andromeda.config.prefix

    -- starts with prefix
    if message:sub(1, #prefix) == prefix then
        local parts = split_words(message:sub(#prefix + 1))
        if #parts == 0 then return end

        local command = find(commands, predicate.prop_contains("aliases", parts[1]:lower()))
        
        if not command then return end -- make sure command exists

        if rank.player(player).rank < command.rank and not ignore_rank then -- make sure player has permission
            tablet.warn(player, "You do not have permission to execute this command.")
            return
        end

        local raw_args = after(parts, 2)

        local flags = {}

        -- parse flags
        for i,v in next, command.flags or {} do
            local match, match_index = find(raw_args, function(a)
                for _,al in next, v.aliases do
                    if #al > 1 and a:lower() == "--" .. al then return true
                    elseif #al == 1 and a:lower() == "-" .. al then return true
                    end
                end
                return false
            end)

            if match then
                if v.type == "flag" then
                    flags[v.name] = true
                    remove_at(raw_args, match_index)
                else
                    local parse_success, result = parse_argument(player, v, raw_args[match_index + 1])
                    if not parse_success then
                        tablet.warn(player, "The flag \"" .. v.name .. "\" failed to parse. It expects the type of \"" .. v.type .. "\"." .. (result and "\n" .. result or ""))
                        return
                    end

                    flags[v.name] = result
                    remove_at(raw_args, match_index, match_index + 1)
                end
            end
        end

        local args = {}

        for i,v in next, command.args or {} do
            if v.required and not raw_args[i] then
                tablet.warn(player, "The argument \"" .. v.name .. "\" was not passed, and is required at position " .. i .. ".")
                return
            end

            if raw_args[i] then
                local raw = raw_args[i]
                if i == #command.args and (command.join_last_argument == nil or command.join_last_argument) then raw = table.concat(after(raw_args, i), " ") end
                local parse_success, result = parse_argument(player, v, raw)
                if not parse_success then tablet.warn(player, "The argument \"" .. v.name .. "\" failed to parse. It expects the type of \"" .. v.type .. "\"." .. (result and "\n" .. result or "")) return end
                args[v.name] = result
            end
        end

        local s, e = pcall(function()
            command:func {
                command = command,
                player = player,
                message = message,
                raw_args = raw_args,
                args = args,
                flags = flags
            }
        end)
        if not s and e then
            warn(e)
            tablet.warn(player, "The command issued an uncaught error.")
        end
    end
end

initialize_player = function(player)
    local r = rank.player(player).rank

    -- check banned
    if r == -1 then
        player:Kick("You have been banned from the game by an Andromeda moderator.")
    end

    -- check whitelist
    if not command("whitelist"):check_player(player) then return end

    player.Chatted:connect(function(message)
        parse_chat(player, message)
    end)

    if r > 0 then
        tablet.add(player, "Andromeda initialized.\nYou're rank " .. rank.format(r) .. ".")
    end
end

--------------------------------------------------------------------

service("RunService").Heartbeat:connect(function(delta)
    tablet.update(delta)
    shield.update(delta)
end)
service("Players").PlayerAdded:connect(initialize_player)

each(service("Players"):GetPlayers(), initialize_player)

play_sound(andromeda.config.sounds.new_group)

print("Andromeda initialized.")